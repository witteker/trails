package com.github.erikwittek.Trails;

import java.util.Random;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlayerMoveListener implements Listener {

	private Random random = new Random();

	// private Logger log = Trails.log;

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent evt) {

		Location loc = evt.getPlayer().getLocation();
		loc.setY(loc.getY() - 1);

		World w = loc.getWorld();
		Block b = w.getBlockAt(loc);

		if (b.getTypeId() == 2) {

			// log.info("Block is grass");

			if (random.nextInt(30) == 0) {

				// log.info("random is 0");

				b.setTypeId(3);

				// log.info("id should be set to 3");
			}
		}
	}
}
