package com.github.erikwittek.Trails;

import java.util.logging.Logger;

import org.bukkit.plugin.java.JavaPlugin;

public class Trails extends JavaPlugin {

	public static Logger log;
	private PlayerMoveListener listener;
	
	public void onEnable() {
		
		log = this.getLogger();
		
		listener = new PlayerMoveListener();
		getServer().getPluginManager().registerEvents(listener, this);
		
		log.info("Trails has been enabled!");
	}
	
	public void onDisable() {
		
		log.info("Trails has been disabled!");
	}
}
